cv.service("serviceBookOnlineVenuesFor", function ($http) {
	this.bovfData = function(){
		var serData = {};
		$http.get('data/bookOnlineVenue.json').success(function(res) {
			serData = res; 
		});
		return serData;
	}
});