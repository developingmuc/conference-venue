// Controller for home
cv.controller("ctrHome", function ($scope, $http) {});

//menu controller
cv.controller('ctrMainMenu', function($scope) {
	$scope.menus = [{
		'menuId': 1,
		'menuItem': 'Home',
		'menuItemUrl': '#/'
	}, {
		'menuId': 2,
		'menuItem': 'About us',
		'menuItemUrl': '#/about'
	}, {
		'menuId': 3,
		'menuItem': 'Finding The Right Venue',
		'menuItemUrl': '#/'
	}, {
		'menuId': 4,
		'menuItem': 'Recruitment',
		'menuItemUrl': '#/'
	}, {
		'menuId': 5,
		'menuItem': 'Faqs',
		'menuItemUrl': '#/'
	}, {
		'menuId': 6,
		'menuItem': 'Contacts',
		'menuItemUrl': '#/'
	}];
	$scope.selected = 0;

	$scope.select= function(index) {
		$scope.selected = index; 
	};
});

// rating controller
cv.controller('ctrRating', function ($scope) {
	$scope.ratings = [{
		current: 3,
		max: 5  
	}];
});

// Controller for About
cv.controller("ctrAbout", function ($scope, $timeout) {
	$scope.clock = "loading clock..."; // initialise the time variable
	$scope.tickInterval = 1000; //ms

	var tick = function() {
		$scope.clock = Date.now() // get the current time
		$timeout(tick, $scope.tickInterval); // reset the timer
	}
	// Start the timer
	$timeout(tick, $scope.tickInterval);
});


// Controller for Listing Page
cv.controller("ctrListing", function ($scope, $http) {
	$http.get('data/hotelListing.json').success(function(data) {
		$scope.listingData= data;
	});
});


// Controller for Detail Page
cv.controller("ctrDetailPage", function ($scope, $http) {
	$http.get('data/venueDetailPage.json').success(function(data) {
		console.log(JSON.stringify(data));
		$scope.venueDetailPageData= data;
	});

	$scope.tabs = [{
		"title":"General Info",
		"content":"tab_general_info.html"
	}, {
		"title":"Access ",
		"content":"tab_general_info.html"
	}, {
		"title":"Room Detail",
		"content":"tab_general_info.html"
	}, {
		"title":"Hall Detail",
		"content":"tab_general_info.html"
	}, {
		"title":"Facilities",
		"content":"tab_general_info.html"
	}, {
		"title":"Local Attraction",
		"content":"tab_general_info.html"
	}];
});