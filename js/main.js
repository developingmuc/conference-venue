// hover effect top menu
$('.cv-top-nav .cv-top-menu li > a ').hover(function(){
	$(this).find('i').css("transform","scale(1.2)");
},function(){
	$(this).find('i').css("transform","scale(1)");
});

function initialize() {
	var mapProp = {
		center:new google.maps.LatLng(12.899972, 77.575119),
		zoom:12,
		mapTypeId:google.maps.MapTypeId.ROADMAP
	};
	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);