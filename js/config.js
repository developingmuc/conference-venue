cv.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'home.html',
			controller: 'ctrHome'
		}).when('/about', {
			templateUrl: 'about.html',
			controller: 'ctrAbout'
		}).when('/venue/detail', {
			templateUrl: 'detail_page.html',
			controller: ''
		}).when('/listing', {
			templateUrl: 'listing.html',
			controller: 'ctrListing'
		}).otherwise({
			redirectTo: '/'
		});
	}
]);