// directive for rating
cv.directive('starRating', function () {
	return {
		restrict: 'EA',
		template: [
			'<ul class="rating">',
				'<li ng-repeat="star in stars" ng-class="star">',
					'<i class="fa fa-star"></i>',
				'</li>',
			'</ul>'
		].join(''),
		scope: {
			ratingValue: '=',
			max: '='
		},
		link: function (scope, elem, attrs) {
			scope.stars = [];
			for (var i = 0; i < scope.max; i++) {
				scope.stars.push({
					filled: i < scope.ratingValue
				});
			}
		}
	}
});

// directive for book online venue for
cv.directive('bookOnlineVenueFor',function(){
	return {
		restrict:'EA',
		templateUrl:'book_online_venue_for.html',
		controller:function($scope, $http){
			$http.get('data/bookOnlineVenue.json').success(function(data) {
				$scope.bookOnlineVenueForData= data;
			});
		}
		/*
		controller:function($scope, serviceBookOnlineVenuesFor){
			$scope.bookOnlineVenueForData= serviceBookOnlineVenuesFor.bovfData;
			// $http.get('data/bookOnlineVenue.json').success(function(data) {
			//       $scope.bookOnlineVenueForData= data;
			// });
		}*/
	}
});

// directive for featured venues
cv.directive('featuredVenues',function(){
	return {
		restrict:'EA',
		templateUrl:'featured_venues.html',
		controller:function($scope, $http){
			$http.get('data/featuredVenues.json').success(function(data) {
				//console.log(JSON.stringify(data));
				$scope.featuredVenuesData= data;
			});
		}
	}
});

// directive for hot deals
cv.directive('hotDeals',function(){
	return {
		restrict:'EA',
		templateUrl:'hot_deals.html',
		controller:function($scope, $http){
			$http.get('data/hotDeals.json').success(function(data) {
				//console.log(JSON.stringify(data));\
				$scope.hotDealData= data;
			});
		}
	}
});

// directive for hotels
cv.directive('hotel',function(){
	return {
		restrict:'EA',
		templateUrl:'hotel.html'
	}
});